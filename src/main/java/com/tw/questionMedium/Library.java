package com.tw.questionMedium;

import java.util.*;
import java.util.stream.Collectors;

public class Library {
    private final List<Book> books = new ArrayList<>();

    public void addAll(Book ...books) {
        this.books.addAll(Arrays.asList(books));
    }

    /**
     * Find all the books which contains one of the specified tags.
     *
     * @param tags The tag list.
     * @return The books which contains one of the specified tags. The returned books
     *   should be ordered by their ISBN number.
     */
    public List<Book> findBooksByTag(String ...tags) {
        // TODO:
        //   Please implement the method
        // <-start-
        List<Book> foundBooks = new ArrayList<>();
        for (int i = 0; i < books.size(); i++) {
            for (int j = 0; j < tags.length; j++) {
                if (books.get(i).getTags().contains(tags[j])) {
                    foundBooks.add(books.get(i));
                }
            }
        }

        Collections.sort(foundBooks, Comparator.comparing(Book::getIsbn));

        List<Book> singleSortedFoundBooks = foundBooks.stream().distinct().collect(Collectors.toList());
        return singleSortedFoundBooks;
        // --end-->
    }

    // TODO:
    //   You can add additional methods here if you want.
    // <-start-

    // --end-->
}
