package com.tw.questionEasy;

public class HtmlEscaper {
    // TODO:
    //   You can add additional members or blocks of code here if you want.
    // <-start-
    
    // --end-->

    /**
     * This function will try escaping characters according to the rules defined in HTML 4.01
     * The rules are as follows:
     *
     * (1) Every `"` character will be escaped to `&quot;`
     * (2) Every `'` character will be escaped to `&#39;`
     * (3) Every `&` character will be escaped to `&amp;`
     * (4) Every `<` character will be escaped to `&lt;`
     * (5) Every `>` character will be escaped to `&gt;`
     *
     * @param text The text to escape.
     * @return The escaped string.
     */
    public static String escape(String text) {
        // TODO:
        //   Please implement the method
        // <-start-
        if (text == null) {
            throw new IllegalArgumentException();
        }

        StringBuilder stringBuilder = new StringBuilder(text);
        for (int i = 0; i < stringBuilder.length(); i++) {
            if (stringBuilder.charAt(i) == '&') {
                stringBuilder.replace(i, i + 1, "&amp;");
            } else if (stringBuilder.charAt(i) == '\"') {
                stringBuilder.replace(i, i + 1, "&quot;");
            } else if (stringBuilder.charAt(i) == '\'') {
                stringBuilder.replace(i, i + 1, "&#39;");
            } else if (stringBuilder.charAt(i) == '<') {
                stringBuilder.replace(i, i + 1, "&lt;");
            } else if (stringBuilder.charAt(i) == '>') {
                stringBuilder.replace(i, i + 1, "&gt;");
            }
        }
        return stringBuilder.toString();
        // --end-->
    }
}
